#!/usr/bin/env node

import { createConnection } from 'net';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { spawn } from 'child_process';
import { tmpdir } from 'os';

const args = yargs(hideBin(process.argv))
  .option('start', {
    describe: 'Start playback server'
  })
  .option('stop', {
    describe: 'Stop playback server'
  })
  .option('volume', {
    describe: 'Sets the volume of the playback (0-100 or %)',
    requiresArg: true
  })
  .option('mute', {
    describe: 'Mutes playback'
  })
  .option('unmute', {
    describe: 'Unmutes playback'
  })
  .epilogue('This program was not written with super cow powers.')
  .conflicts('mute', 'unmute')
  .help('h')
  .alias('h', 'help')
  .alias('V', 'version')
  .parseSync();

if (args.start) {

  const proc = spawn('node', ['./build/server/pm2-control.js']);
  proc.on('spawn', () => console.log('Server started successfully'));
  proc.on('error', console.error);

} else {
  const socket = createConnection(`${tmpdir()}/scruffy-ac.socket`);

  socket.on('ready', () => {
    console.log(`Connected to socket ${tmpdir()}/scruffy-ac.socket`);

    //console.log(args);

    if (args.stop) {
      console.log('Stopping server...');
      const proc = spawn('pm2', ['stop', 'scruffy-ac']);
      proc.on('spawn', () => console.log('Server stopped successfully'));
      proc.on('error', console.error);
    }

    // volume can be 0 which is falsey
    if (args.volume !== undefined) {
      console.log('Sending volume', args.volume);
      socket.write(Buffer.from(`GAIN ${args.volume}`));
    }

    if (args.mute) {
      console.log('Muting');
      socket.write(Buffer.from('GAIN 0'));
    }

    if (args.unmute) {
      console.log('Unmuting');
      socket.write(Buffer.from('GAIN 30'));
    }

    console.log('Done. Disconnecting...');
    socket.end();
  });

  interface ExtendedError extends Error {
    code?: string;
  }
  socket.on('error', (error: ExtendedError) => {
    if (error.code === 'ENOENT') {
      console.error('Socket does not exist. Is the server running?');
      process.exit(1);
    } else
      throw error;
  });
}
