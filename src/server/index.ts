if (Number(process.version.split('.')[0].substring(1)) < 14)
  throw new Error('You must upgrade to at least node 14.');

// console.log('server called');

import getTimes from './getTimes';
import Player from './player';
import Socket from './socket';

const player = new Player(30, false);
const socket = new Socket();

// start the first song
const { hour, frameOffset } = getTimes();
//const hour = '12', frameOffset = 138000;

// wait for the player to become ready before spawning
player.player.on('spawn', () => {
  player.playSong(hour, frameOffset);
});

// then start the next songs
player.player.on('endSong', () => {
  console.log('Current hour\'s song has ended. Playing next hour\'s...');

  const { hour: newHour, frameOffset: newFrameOffset } = getTimes();
  //const newHour = '13', newFrameOffset = 138000;
  player.playSong(newHour, newFrameOffset);
});

socket.on('message', (message: string) => {
  console.log('New message:', message);
  player.sendCommand(message);
});

process.on('SIGINT', () => {
  if (socket.server) socket.server.close();
  console.log('\nClosed socket');
  player.sendCommand('QUIT');

  process.exit();
});

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
process.stdin?.pipe(player.player.stdin!);