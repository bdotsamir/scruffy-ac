/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { ChildProcess, spawn } from 'child_process';

class Player {
  public player: ChildProcess;
  public volume: number;

  constructor(volume = 100, verbose = false) {

    this.player = spawn('mpg123', ['-R', '.']);
    this.volume = volume;

    this.player.on('spawn', () => {
      console.log('Player spawned successfully');
    });

    this.player.on('error', err => {
      throw err;
    });

    this.player.stdout!.on('data', (data: Buffer | string) => {
      data = data.toString();

      // ignore playback output (it's just verbose frames and stuff)
      if (data.startsWith('@F') && /@F \d+ \d+ \d+\.\d+ \d+\.\d+/.test(data) && verbose === false)
        return;

      console.log(data);
      // player has reached the end of the current song.
      // emit the endSong event for the index file to handle.
      if (data.trim() === '@P 3') {
        console.log('Song ended');
        this.player.emit('endSong');
      }
    });

    this.player.stderr!.on('data', (data: Buffer | string) => {
      data = data.toString();

      if (data.startsWith('@F') && /@F \d+ \d+ \d+\.\d+ \d+\.\d+/.test(data) && verbose === false)
        return;

      console.error('player error:', data);
    });
  }

  public playSong(hour: string, frameOffset: number): void {
    console.log('Playing song', hour, 'at frame offset', frameOffset);

    this.sendCommand(`LOAD ./assets/${hour}.mp3`);
    this.sendCommand(`JUMP ${frameOffset}`);
    if (this.volume !== 100) this.sendCommand(`GAIN ${this.volume}`);
  }

  public sendCommand(command: string): boolean {
    if (command.startsWith('GAIN')) this.volume = +(command.substring(7).trim());

    return this.player.stdin!.write(command + '\n');
  }
}

export default Player;