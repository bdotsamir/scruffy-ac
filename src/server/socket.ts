import { EventEmitter } from 'events';
import { createServer, Server } from 'net';
import { tmpdir } from 'os';

// way overcomplicated but i dont really care?
// i cant stop thinking in classes. it's a problem.
// https://gist.github.com/lolzballs/2152bc0f31ee0286b722
class Socket extends EventEmitter {
  public server: Server;

  constructor() {
    super();

    this.server = createServer();
    this.server.listen(`${tmpdir()}/scruffy-ac.socket`, () => { console.log(`Listening on socket ${tmpdir()}/scruffy-ac.socket`); });

    this.server.on('connection', socket => {
      console.log('New socket connection');

      socket.on('data', (data: Buffer | string) => {
        data = data.toString();
  
        this.emit('message', data);
      });

      socket.on('close', () => { console.log('Socket disconnected'); });
    });

    this.server.on('error', (error: Error) => {
      throw error;
    });
  }
}

export default Socket;

/*
export function socket(): void {
  server = createServer();
  server.listen('/tmp/scruffy-ac.socket', () => { console.log('Listening on socket'); });

  server.on('connection', socket => {
    socket.on('data', (data: Buffer | string) => {
      data = data.toString();

      console.log(data);
    });
  });
}

process.on('SIGINT', () => {
  server.close();

  process.exit();
});
*/