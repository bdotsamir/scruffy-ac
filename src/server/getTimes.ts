export default function getTimes(): { hour: string, frameOffset: number } {
  const currentTime = new Date();

  const hour = currentTime.getHours().toString();
  //const hour = '14';
  const minute = currentTime.getMinutes();
  //const minute = 59;
  const second = currentTime.getSeconds();
  //const second = 58;

  // convert minutes+seconds to milliseconds
  const milliseconds = (minute * 60 + second) * 1000;

  console.log('hour', +hour);
  console.log('minute', minute);
  console.log('second', second);

  // convert milliseconds to mp3 frames
  const sampleRate = 44100; // hz
  const frameLengthInMs = (1152 / sampleRate) * 1000;

  /* equation:
  frameLengthInMs ms      x milliseconds
  -----------------   =  ----------------
     1 frame                y frames
  */

  const frameOffset = milliseconds / frameLengthInMs;

  return { hour, frameOffset };
}