# scruffy-ac
24/7 Animal Crossing music provided by the lovely [Scruffy](https://www.youtube.com/channel/UCDoKh1xoqKDfUr3YxQBs_1w) on youtube.
Find the playlist [here](https://www.youtube.com/playlist?list=PLQY6PNl6p3MYWVZa5R5Emdi-CxgM40Scm).

This project is meant to be run on a raspberry pi, but it will run anywhere node.js will install. As long as you have a speaker.

## Requirements
* **This program requires [git lfs](https://git-lfs.github.com/) if you plan on cloning it.**
* You'll require the system package `mpg123`- you can find this in your distro's package manager.
* You'll also need [node.js](https://nodejs.org) (unless i've taken the time to distribute executables).

## Getting started
> This guide is written for Linux users. It *may* work for other OS's, but it is not guaranteed.
1. Run `yarn` or `npm i` (depending on your node package manager)
2. Run `yarn build` or `npm build` to compile the project into javascript
3. Run `yarn start` or `npm run start`. Use this for any further starts on this version (`0.0.1`).
4. Leave it running :)

## [LICENSE.md](./LICENSE.md)
[![image](https://user-content.gitlab-static.net/48933d2d293d616c27551ba9a8a6603380584c33/68747470733a2f2f692e696d6775722e636f6d2f754b316e3631362e706e67)](https://choosealicense.com/licenses/mit)

## [CHANGELOG.md](./CHANGELOG.md)